output "instance_id" {
  value = google_compute_instance.tf-chapter12.instance_id
}

output "cpu_platform" {
  value = google_compute_instance.tf-chapter12.cpu_platform
}
